#!/bin/sh
cd "$(readlink -f "$(dirname "$0")")"
[ $# -gt 0 ] || set -- target
dibs -A "$@"
