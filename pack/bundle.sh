#!/bin/sh
exec >&2
set -eu
user="$1"
group="$2"
cachedir="$(cat DIBS_DIR_CACHE)/root"

# Copy everything from the cache
tar cC "$cachedir" . | tar xC /
chown -R "$user:$group" /app
