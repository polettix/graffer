#!/bin/sh
exec >&2
set -eu
cachedir="$(cat DIBS_DIR_CACHE)/root"
srcdir="$(cat DIBS_DIR_SRC)"

pip3 install --user cairosvg
rm -rf /app/.cache

# Copy everything to the cache
rm -rf "$cachedir"
mkdir -p "$cachedir/app"

tar cC "$srcdir" . | tar xC "$cachedir"
tar cC /app .      | tar xC "$cachedir/app"
