# graffer

A simple [Docker][] image with [Alpine Linux][] inside, for using some graph programs from the command line. Stuff will be added along time.

Install it with the following command:

```shell
docker pull registry.gitlab.com/polettix/graffer
docker tag registry.gitlab.com/polettix/graffer graffer:active
docker run --rm graffer:active --wrapper 2>/dev/null >graffer
chmod +x graffer
printf '%s\n' 'now put graffer in some place into PATH...'
```

If you are brave, you can just run this:

```shell
curl -L https://gitlab.com/polettix/graffer/-/raw/master/install | sh
```

You can then create symbolic links to this program to directly call the programs inside:

- `cairosvg`
- `dot` and its friends from [Graphviz][].
- `gs` and related programs
- `magick` and its siblings (e.g. `convert`, `mogrify`, ...) from
  [ImageMagick][]
- `pdfjoin`, a wrapper to `gs` for merging PDF files together (output on
  standard output)
- `qrencode` from [libqrencode][]
- `zbarimg` from [zbar][]
- [qrate][]

Get all of them with this:

```
for program in cairosvg dot gs magick pdfjoin qrencode zbarimg qrate ; do
    ln -s graffer "$program"
done
```

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.


[Docker]: https://www.docker.com/
[Alpine Linux]: https://www.alpinelinux.org/
[graffer]: https://gitlab.com/polettix/graffer/-/blob/master/graffer
[Graphviz]: https://graphviz.org/
[ImageMagick]: https://imagemagick.org/index.php
[libqrencode]: https://fukuchi.org/works/qrencode/
[zbar]: http://zbar.sourceforge.net/
[qrate]: https://github.polettix.it/ETOOBUSY/2022/04/18/qrate/
